/*
 *  Pareu de Pararme map
 *
 *  Copyright (C) 2021 SOS Racisme
 *
 *  This program is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU Affero General Public License as published by the
 *  Free Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *  FOR A PARTICULAR PURPOSE.
 *  See the GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 *  SPDX-License-Identifier: AGPL-3.0-only
 *
 */

import 'package:flutter_map/flutter_map.dart';
import 'package:pareu_de_pararme_map/ui/widgets/non_foss/location_button.dart';

/// This is a patch to easly have two versions of the app, one foss and other
/// not.
///
/// Comment part of the code to enable one or the other.
// todo: search a beautiful way to do that
List<LayerOptions> getLocationButton(MapController _mapController, ) {
  return [getLocationButtonNonFOSS(_mapController)];
  return <LayerOptions>[];
}

List<MapPlugin> getMapPlugins(){
  return <MapPlugin>[getLocationPlugin()];
  return <MapPlugin>[];
}


