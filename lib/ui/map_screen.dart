/*
 *  Pareu de Pararme map
 *
 *  Copyright (C) 2021 SOS Racisme
 *
 *  This program is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU Affero General Public License as published by the
 *  Free Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *  FOR A PARTICULAR PURPOSE.
 *  See the GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 *  SPDX-License-Identifier: AGPL-3.0-only
 *
 */

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:pareu_de_pararme_map/AppBuilder.dart';
import 'package:pareu_de_pararme_map/models/alert.dart';
import 'package:pareu_de_pararme_map/models/panel.dart';
import 'package:pareu_de_pararme_map/service/alerts.dart';
import 'package:pareu_de_pararme_map/ui/constants.dart';
import 'package:pareu_de_pararme_map/ui/widgets/alert_form.dart';
import 'package:pareu_de_pararme_map/ui/widgets/home/home_screen.dart';
import 'package:pareu_de_pararme_map/ui/widgets/home/models.dart';
import 'package:pareu_de_pararme_map/ui/widgets/map_widget.dart';
import 'package:pareu_de_pararme_map/ui/widgets/trail_icon.dart';
import 'package:provider/provider.dart';
import 'package:pareu_de_pararme_map/generated/l10n.dart';


class MapScreen  extends StatefulWidget {
  @override
  MapScreen_State createState() => MapScreen_State();
}

class MapScreen_State extends State<MapScreen> {
  // Main screen, tested with two tabs and work good
  var _tabs = <TabInterface>[ ];

  late Timer _updateTimer;

  /// Timer that check on an interval if new alerts have to be added
  Timer getUpdateTimer([Duration updateInterval = UPDATE_INTERVAL]) =>
    Timer.periodic(updateInterval, (Timer t) async {
      if(mounted) {
        print("Updating automatically with range $alertsUpdateDateRange");
        context.read<AlertsListModel>().add(
            await getAlerts(rangeDates: alertsUpdateDateRange)
        );
      }
    });



  @override
  void initState() {
    _updateTimer = getUpdateTimer();
    _tabs.add(TabInterface("", child: MainMap( )),);
    super.initState();
  }

  @override
  void dispose() {
    _updateTimer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return
      ChangeNotifierProvider(create: (context) => NewAlertModel(),
        child: HomeScreen(
          topBarTitle: S.of(context).addAlert,
          leadingImage: Intl.getCurrentLocale().contains("CA") ? "assets/images/logo-light-ca.png" : "assets/images/logo-light-es.png",
          kAppBarMinHeight: 80,
          kAppBarMidHeight: 100,
          buttonHeight: 40,
          topbarBackgroundColor: PRIMARY_NAV_COLOR,
          topbarBackgroundColor2: PRIMARY_NAV_COLOR,
          topbarChild: AlertForm(),
          tabs: _tabs,
          disableBottomShadow: true,
          trailIcon: trailIcon(),
          panelController: panelController,
          enableBottomBar: false,
          onPanelClosed: () => context.read<PanelModel>().setCloseState(),
          onPanelOpened: () {

            context.read<PanelModel>().setOpenedState();
          },
          floatingActionButton: FloatingActionButton(
            heroTag: "tagtag", // Used to prevent Error: There are multiple heroes that share the same tag within a subtree.
            onPressed: () {
              var panelModel = context.read<PanelModel>();
              panelModel.isOpen
                  ? panelModel.close()
                  : panelModel.open();
            },
            child: Icon( panelController.isAttached && context.watch<PanelModel>().isOpen
                ? Icons.campaign_outlined : Icons.campaign,
              size: 35,
              color: Colors.white,
            ),
            backgroundColor: !context.watch<PanelModel>().isOpen ? PRIMARY_BTN_COLOR : PRIMARY_HOVER_BTN_COLOR,
            hoverColor: PRIMARY_HOVER_BTN_COLOR,
            focusColor: PRIMARY_HOVER_BTN_COLOR,
          ),
        ),
      );
  }
}
