/*
 *  Pareu de Pararme map
 *
 *  Copyright (C) 2021 SOS Racisme
 *
 *  This program is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU Affero General Public License as published by the
 *  Free Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *  FOR A PARTICULAR PURPOSE.
 *  See the GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 *  SPDX-License-Identifier: AGPL-3.0-only
 *
 */

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:pareu_de_pararme_map/ui/constants.dart';
import 'package:pareu_de_pararme_map/ui/widgets/home/models.dart';
import 'package:pareu_de_pararme_map/ui/widgets/home/topbar.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';

class HomeScreen extends StatefulWidget {
  Color tabColorAnimationBegin, tabColorAnimationEnd;
  IconData leftIcon, rightIcon;
  bool enableBottomBar;
  FloatingActionButton floatingActionButton;
  List<TabInterface> tabs;

  /// This height define the total height when opened
  double buttonHeight ;
  double personDelegateHeight;
  double appBarHeight;

  /// This regulate the minimum height of topbar, this is when topbar is closed
  double kAppBarMinHeight ;
  double kAppBarMidHeight ;
  double homeScreenBottomBarHeight;

  /// Title under the leading icon on topbar
  String topBarTitle;
  /// Top bar leading image/icon
  IconData leadingIcon;
  /// If [leadingImage] is defined [leadingIcon] will be ignored
  String leadingImage;

  /// Widget that will be shown when topbar is opened
  Widget topbarChild;
  /// Topbar background color
  Color topbarBackgroundColor;
  Color topbarBackgroundColor2;
  /// Controller for panel, util to open and close it
  late PanelController panelController;
  /// Icon located on the top left of the panel
  Widget? trailIcon;
  /// If true trail icon will be hide
  bool disableTrailIcon;

  final bool disableBottomShadow;

  void Function()? onPanelClosed, onPanelOpened;


  HomeScreen(
      {Key? key,
        required this.tabs,
        required this.topbarChild,
        required this.floatingActionButton,
        this.leadingIcon =  Icons.person,
        this.leadingImage = "",
        this.tabColorAnimationBegin = Colors.lightBlueAccent,
        this.tabColorAnimationEnd =  Colors.black12,
        this.leftIcon = Icons.chat_bubble_outline,
        this.rightIcon = Icons.people_outline,
        this.enableBottomBar = true,

        this.buttonHeight = 55,
        this.personDelegateHeight = 66,
        this.appBarHeight = 56.0,
        this.kAppBarMinHeight = 110.0,
        this.kAppBarMidHeight = 256.0,
        this.homeScreenBottomBarHeight = 50.0,
        this.topBarTitle = "",
        this.topbarBackgroundColor = Colors.white,
        this.topbarBackgroundColor2 = Colors.white,
        required this.panelController,
        this.trailIcon,
        this.disableTrailIcon = false,
        this.disableBottomShadow = false,
        this.onPanelOpened,
        this.onPanelClosed
      }) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> with TickerProviderStateMixin {
  late TabController _tabController;
  late PanelController _panelController;

  late Animation<Color?> _leftIconAnimation;
  late Animation<Color?> _rightIconAnimation;

  late Animation<Color?> shadowColor;
  late AnimationController _animationController;

  late IconData leftIcon, rightIcon;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(vsync: this, length: widget.tabs.length);
    // _panelController = widget.panelController ?? PanelController();
    _panelController = widget.panelController;
    _leftIconAnimation =
        ColorTween(begin: widget.tabColorAnimationBegin, end: widget.tabColorAnimationEnd)
            .animate(_tabController.animation!);

    _rightIconAnimation =
        ColorTween(begin: widget.tabColorAnimationEnd, end: widget.tabColorAnimationBegin)
            .animate(_tabController.animation!);

    _animationController =
        AnimationController(vsync: this, duration: Duration(milliseconds: 200));

    if(widget.enableBottomBar) {
      leftIcon = widget.leftIcon;
      rightIcon = widget.rightIcon;
    }
    shadowColor = ColorTween(
      begin: Color.fromRGBO(0, 0, 0, 0),
      end: Colors.black12,
    ).animate(_animationController);
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  Widget getLeftIconBuilder(BuildContext context, Widget? widget) {
    return Icon(leftIcon,
        color: _leftIconAnimation.value, size: 30);
  }

  Widget getRightIconBuilder(BuildContext context, Widget? widget) {
    return Icon(rightIcon,
        color: _rightIconAnimation.value, size: 30);
  }

  Widget _body(double topBarMinHeight) {
    return Column(
      children: <Widget>[
        SizedBox(
          height: topBarMinHeight,
        ),
        // Uncoment this to append a search bar
        // Hero(
        //   tag: 'search_box',
        //   child: Material(
        //     color: Colors.white,
        //     child: GestureDetector(
        //       onTap: () {
        //         Navigator.pushNamed(context, '/search', arguments: _tabController.index,);
        //       },
        //       child: Container(
        //         decoration: BoxDecoration(
        //           borderRadius: BorderRadius.circular(15),
        //           color: Color(0xFFF5F5F5),
        //         ),
        //         padding: const EdgeInsets.symmetric(horizontal: 15),
        //         margin: EdgeInsets.symmetric(horizontal: 8),
        //         height: 40,
        //         child: Align(
        //           alignment: Alignment.centerLeft,
        //           child: Row(
        //             children: <Widget>[
        //               Icon(Icons.search,
        //                   color: Theme.of(context).textTheme.body1!.color),
        //               SizedBox(
        //                 width: 8,
        //               ),
        //               Expanded(
        //                 child: Text(
        //                   'Type text...',
        //                   style: Theme.of(context).textTheme.body2!.copyWith(color: Theme.of(context).hintColor),
        //                 ),
        //               ),
        //             ],
        //           ),
        //         ),
        //       ),
        //     ),
        //   ),
        // ),
        Expanded(
          child:
          TabBarView(
            controller: _tabController,
            children: widget.tabs
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    final MediaQueryData mediaQueryData = MediaQuery.of(context);
    final double statusBarHeight = mediaQueryData.padding.top;
    final double screenHeight = mediaQueryData.size.height;
    final double appBarMinHeight = widget.kAppBarMinHeight - statusBarHeight;
    final double appBarMaxHeight = appBarMinHeight +
        (screenHeight - statusBarHeight) * 0.15 +
        5 * widget.buttonHeight +
        20;

    return Scaffold(
      body: SafeArea(
        top: true,
        bottom: true,
        child: SlidingUpPanel(
          // color: widget.topbarBackgroundColor2,
          color: PANEL_BOTTOM,
          controller: _panelController,
          maxHeight: appBarMaxHeight,
          minHeight: widget.kAppBarMinHeight,
          parallaxEnabled: true,
          parallaxOffset: .5,
          slideDirection: SlideDirection.DOWN,
          backdropEnabled: false,
          body: _body(widget.kAppBarMinHeight),
          onPanelClosed: widget.onPanelClosed,
          onPanelOpened: widget.onPanelOpened,
          panel: TopBar(
            backgroundColor: widget.topbarBackgroundColor,
            backgroundColor2: widget.topbarBackgroundColor2,
            topbarChild: widget.topbarChild,
            leadingIcon: widget.leadingIcon,
            leadingImage: widget.leadingImage,
            title: widget.topBarTitle,
            maxHeight: appBarMaxHeight,
            minHeight: widget.kAppBarMinHeight,
            tabController: _tabController,
            panelAnimationValue: _animationController.value,
            panelController: _panelController,
            firstTitle: widget.tabs[0].tabTitle,
            secondTitle: widget.tabs.length > 1 ? widget.tabs[1].tabTitle : "",
            buttonHeight: widget.buttonHeight,
            trailIcon: widget.trailIcon,
            disableTrailIcon: widget.disableTrailIcon,
            disableBottomShadow: widget.disableBottomShadow,
          ),
          boxShadow: <BoxShadow>[
            BoxShadow(
              color: shadowColor.value!,
              blurRadius: 20.0,
              spreadRadius: 5.0,
              offset: Offset(
                0.0,
                15.0,
              ),
            ),
          ],
          padding: EdgeInsets.only(
              bottom: _animationController.value * widget.appBarHeight / 3),
          borderRadius: BorderRadius.only(
              bottomLeft: Radius.circular(
                  _animationController.value * widget.appBarHeight / 3),
              bottomRight: Radius.circular(
                  _animationController.value * widget.appBarHeight / 3)),
          onPanelSlide: (double pos) => setState(
            () {
              _animationController.value = pos;
            },
          ),
        ),
      ),
      bottomNavigationBar: widget.enableBottomBar ?
      BottomAppBar(
        child: Stack(
          children: <Widget>[
            Container(
              height: widget.homeScreenBottomBarHeight,
              child: Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  Expanded(
                    child: GestureDetector(
                      onTap: () {
                        _tabController.animateTo(0);
                      },
                      child: Container(
                        color: Colors.white,
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            AnimatedBuilder(
                              animation: _tabController.animation!,
                              builder: getLeftIconBuilder,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  SizedBox(width: 74),
                  Expanded(
                    child: GestureDetector(
                      onTap: () {
                        _tabController.animateTo(1);
                      },
                      child: Container(
                        color: Colors.white,
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            AnimatedBuilder(
                              animation: _tabController.animation!,
                              builder: getRightIconBuilder,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            GestureDetector(
              onTap: _panelController.close,
              child: Opacity(
                opacity: _animationController.value * 0.5,
                child: Container(
                  height: 50,
                  color:
                      _animationController.value == 0.0 ? null : Colors.black,
                ),
              ),
            ),
          ],
        ),
        shape: CircularNotchedRectangle(),
        notchMargin: 7,
      ) : null,
      floatingActionButton: Container(
        margin: !widget.enableBottomBar ? EdgeInsets.only(bottom: 20) : null,
        height: 60,
        width: 60,
        child: FittedBox(
          child: widget.floatingActionButton
        ),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
    );
  }
}
