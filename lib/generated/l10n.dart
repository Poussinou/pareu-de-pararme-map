// GENERATED CODE - DO NOT MODIFY BY HAND
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'intl/messages_all.dart';

// **************************************************************************
// Generator: Flutter Intl IDE plugin
// Made by Localizely
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, lines_longer_than_80_chars
// ignore_for_file: join_return_with_assignment, prefer_final_in_for_each
// ignore_for_file: avoid_redundant_argument_values, avoid_escaping_inner_quotes

class S {
  S();

  static S? _current;

  static S get current {
    assert(_current != null,
        'No instance of S was loaded. Try to initialize the S delegate before accessing S.current.');
    return _current!;
  }

  static const AppLocalizationDelegate delegate = AppLocalizationDelegate();

  static Future<S> load(Locale locale) {
    final name = (locale.countryCode?.isEmpty ?? false)
        ? locale.languageCode
        : locale.toString();
    final localeName = Intl.canonicalizedLocale(name);
    return initializeMessages(localeName).then((_) {
      Intl.defaultLocale = localeName;
      final instance = S();
      S._current = instance;

      return instance;
    });
  }

  static S of(BuildContext context) {
    final instance = S.maybeOf(context);
    assert(instance != null,
        'No instance of S present in the widget tree. Did you add S.delegate in localizationsDelegates?');
    return instance!;
  }

  static S? maybeOf(BuildContext context) {
    return Localizations.of<S>(context, S);
  }

  /// `Envia`
  String get send {
    return Intl.message(
      'Envia',
      name: 'send',
      desc: '',
      args: [],
    );
  }

  /// `Ok`
  String get ok {
    return Intl.message(
      'Ok',
      name: 'ok',
      desc: '',
      args: [],
    );
  }

  /// `Actualitza`
  String get update {
    return Intl.message(
      'Actualitza',
      name: 'update',
      desc: '',
      args: [],
    );
  }

  /// `Cancel·la`
  String get cancel {
    return Intl.message(
      'Cancel·la',
      name: 'cancel',
      desc: '',
      args: [],
    );
  }

  /// `Selecciona`
  String get select {
    return Intl.message(
      'Selecciona',
      name: 'select',
      desc: '',
      args: [],
    );
  }

  /// `Afegeix alerta`
  String get addAlert {
    return Intl.message(
      'Afegeix alerta',
      name: 'addAlert',
      desc: '',
      args: [],
    );
  }

  /// `Idioma`
  String get language {
    return Intl.message(
      'Idioma',
      name: 'language',
      desc: '',
      args: [],
    );
  }

  /// `Error de connexió, reintentar`
  String get networkErrorButton {
    return Intl.message(
      'Error de connexió, reintentar',
      name: 'networkErrorButton',
      desc: '',
      args: [],
    );
  }

  /// `Error resolent el captcha`
  String get captchaError {
    return Intl.message(
      'Error resolent el captcha',
      name: 'captchaError',
      desc: '',
      args: [],
    );
  }

  /// `Veure alertes des de: `
  String get showAlertsFrom {
    return Intl.message(
      'Veure alertes des de: ',
      name: 'showAlertsFrom',
      desc: '',
      args: [],
    );
  }

  /// `Formalitzar parada`
  String get formalizeComplaint {
    return Intl.message(
      'Formalitzar parada',
      name: 'formalizeComplaint',
      desc: '',
      args: [],
    );
  }

  /// `Introdueixi el text`
  String get insertText {
    return Intl.message(
      'Introdueixi el text',
      name: 'insertText',
      desc: '',
      args: [],
    );
  }

  /// `Siusplau, insereixi una descripció d\'almenys 5 lletres`
  String get formDescriptionError {
    return Intl.message(
      'Siusplau, insereixi una descripció d\\\'almenys 5 lletres',
      name: 'formDescriptionError',
      desc: '',
      args: [],
    );
  }

  /// `Descripció`
  String get formDescriptionLabel {
    return Intl.message(
      'Descripció',
      name: 'formDescriptionLabel',
      desc: '',
      args: [],
    );
  }

  /// `Tipus de control`
  String get formControlTypeLabel {
    return Intl.message(
      'Tipus de control',
      name: 'formControlTypeLabel',
      desc: '',
      args: [],
    );
  }

  /// `Seleccioni un punt al mapa per enviar l'alerta`
  String get formPointNotSelectedError {
    return Intl.message(
      'Seleccioni un punt al mapa per enviar l\'alerta',
      name: 'formPointNotSelectedError',
      desc: '',
      args: [],
    );
  }

  /// `Publicar Alerta`
  String get formPublishAlert {
    return Intl.message(
      'Publicar Alerta',
      name: 'formPublishAlert',
      desc: '',
      args: [],
    );
  }

  /// `Canvia data`
  String get formChangeDate {
    return Intl.message(
      'Canvia data',
      name: 'formChangeDate',
      desc: '',
      args: [],
    );
  }

  /// `Error de autentificació, reprovi`
  String get formAuthError {
    return Intl.message(
      'Error de autentificació, reprovi',
      name: 'formAuthError',
      desc: '',
      args: [],
    );
  }

  /// `Error de connexió`
  String get formConnectionError {
    return Intl.message(
      'Error de connexió',
      name: 'formConnectionError',
      desc: '',
      args: [],
    );
  }

  /// `Voleu formalitzar la parada?`
  String get formFormalizeComplaintQuestion {
    return Intl.message(
      'Voleu formalitzar la parada?',
      name: 'formFormalizeComplaintQuestion',
      desc: '',
      args: [],
    );
  }

  /// `Seleccioni data de la alerta`
  String get formChangeAlertDate {
    return Intl.message(
      'Seleccioni data de la alerta',
      name: 'formChangeAlertDate',
      desc: '',
      args: [],
    );
  }

  /// `Seleccioni la hora de la alerta`
  String get formChangeAlertHour {
    return Intl.message(
      'Seleccioni la hora de la alerta',
      name: 'formChangeAlertHour',
      desc: '',
      args: [],
    );
  }

  /// `Seleccioni un punt dins de Catalunya`
  String get formPointOutsideBounds {
    return Intl.message(
      'Seleccioni un punt dins de Catalunya',
      name: 'formPointOutsideBounds',
      desc: '',
      args: [],
    );
  }
}

class AppLocalizationDelegate extends LocalizationsDelegate<S> {
  const AppLocalizationDelegate();

  List<Locale> get supportedLocales {
    return const <Locale>[
      Locale.fromSubtags(languageCode: 'es', countryCode: 'CA'),
      Locale.fromSubtags(languageCode: 'es'),
    ];
  }

  @override
  bool isSupported(Locale locale) => _isSupported(locale);
  @override
  Future<S> load(Locale locale) => S.load(locale);
  @override
  bool shouldReload(AppLocalizationDelegate old) => false;

  bool _isSupported(Locale locale) {
    for (var supportedLocale in supportedLocales) {
      if (supportedLocale.languageCode == locale.languageCode) {
        return true;
      }
    }
    return false;
  }
}
