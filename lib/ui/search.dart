/*
 *  Pareu de Pararme map
 *
 *  Copyright (C) 2021 SOS Racisme
 *
 *  This program is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU Affero General Public License as published by the
 *  Free Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *  FOR A PARTICULAR PURPOSE.
 *  See the GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 *  SPDX-License-Identifier: AGPL-3.0-only
 *
 */

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:pareu_de_pararme_map/ui/widgets/top_search_widget.dart';

class SearchPage extends StatefulWidget {
  static const String route = '/';

  @override
  State<StatefulWidget> createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  late TextEditingController textEditingController = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      appBar: AppBar(
          title: TextField(
            controller: textEditingController,
            onEditingComplete: () async {
              FocusScope.of(context).requestFocus(new FocusNode());
            },
            decoration: InputDecoration(
              prefixIcon: Icon(
                Icons.search,
                color: Colors.black,
              ),
              suffix: ValueListenableBuilder<TextEditingValue>(
                valueListenable: textEditingController,
                builder: (ctx, text, child) {
                  if (text.text.isNotEmpty) {
                    return child!;
                  }
                  return SizedBox.shrink();
                },
                child: InkWell(
                  focusNode: FocusNode(),
                  onTap: () {
                    textEditingController.clear();
                    FocusScope.of(context).requestFocus(new FocusNode());
                  },
                  child: Icon(
                    Icons.close,
                    size: 16,
                    color: Colors.black,
                  ),
                ),
              ),
              focusColor: Colors.black,
              filled: true,
              hintText: "search",
              border: InputBorder.none,
              enabledBorder: InputBorder.none,
              fillColor: Colors.grey[300],
              errorBorder: OutlineInputBorder(
                borderSide: BorderSide(color: Colors.red),
              ),
            ),
          ),
        ),
      body: Column(
        children: [
          TopSearchWidget(textEditingController),
        ],
      ),
    );

  }
}
