
/*
 *  Pareu de Pararme map
 *
 *  Copyright (C) 2021 SOS Racisme
 *
 *  This program is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU Affero General Public License as published by the
 *  Free Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *  FOR A PARTICULAR PURPOSE.
 *  See the GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 *  SPDX-License-Identifier: AGPL-3.0-only
 *
 */

import 'package:flutter/material.dart';
import 'package:flutter_map_marker_popup/flutter_map_marker_popup.dart';
import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';

const API_URL =
    'https://api.pareudepararme.org';

const Color PRIMARY_NAV_COLOR = Color(0xffb26955);
const Color SECONDARY_NAV_COLOR =Color(0xffffc861);

const Color PRIMARY_BTN_COLOR = Color(0xff623626);
const Color PRIMARY_HOVER_BTN_COLOR = Color(0xffd67a1d);

const Color SECONDARY_BTN_COLOR = Color(0xffb26955);
const Color SECONDARY_HOVER_BTN_COLOR = Color(0xffd67a1d);

const Color LINK = Colors.blueAccent;
const Color DISABLED = Colors.white54;

const Color PANEL_BOTTOM = Color(0xffbe0000);

const MAP_MARKER_SIZE = 50.0;

const String FORMALIZE_ALERT_URL =
    "https://docs.google.com/forms/d/e/1FAIpQLSdXKagMAbN4UXg2yvyb0MjE_C36lrCJ-uSbAwu8_ZAJqzNdsQ/viewform";

const FCM_NEW_ALERT = "fcm_new_alert";

final PanelController panelController = PanelController();
/// Used to trigger showing/hiding of popups.
final PopupController popupLayerController = PopupController();

// Create storage
final storage = new FlutterSecureStorage();

// Alerts update interval
const UPDATE_INTERVAL = Duration(minutes: 1);
// Range of dates for the update interval
DateTimeRange? alertsUpdateDateRange;