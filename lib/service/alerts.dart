
/*
 *  Pareu de Pararme map
 *
 *  Copyright (C) 2021 SOS Racisme
 *
 *  This program is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU Affero General Public License as published by the
 *  Free Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *  FOR A PARTICULAR PURPOSE.
 *  See the GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 *  SPDX-License-Identifier: AGPL-3.0-only
 *
 */

import 'dart:async';

import 'package:flutter/material.dart';
import 'package:pareu_de_pararme_map/models/alert.dart';
import 'package:pareu_de_pararme_map/models/user_auth.dart';
import 'package:pareu_de_pararme_map/service/base_api.dart';
import 'package:pareu_de_pararme_map/service/user_auth.dart';
import 'package:pareu_de_pararme_map/ui/constants.dart';

const GET_ALERTS = '/api/getalerts';
const CREATE_ALERT = '/api/createalert';

Future<List<Alert>> getAlerts ({DateTimeRange? rangeDates}) async {
  var url = GET_ALERTS;
  if(rangeDates != null) url += "/${rangeDates.start.millisecondsSinceEpoch}/${rangeDates.end.millisecondsSinceEpoch}";
  var alertList = (await apiCall(url,))["res"];
  return alertList.isEmpty ? [] : [
    for (var res in alertList)
      Alert.fromJson(res)
  ];
}

/// Create a new alert
///
/// [context] is needed to update the token in case is outdated
Future<void> createAlert (BuildContext context, UserAuth userAuth, Alert alert,) async {
  if(!userAuth.jwtValidate()) {
    updateToken(context, userAuth);
  }
  await apiCall(CREATE_ALERT, jwtString: userAuth.jwt, params: alert.toJson());
}

// Mockup
// List<Alert> getAlerts = [
//   Alert(
//     time: DateTime.now().millisecondsSinceEpoch,
//     description: "Alert Description",
//     location: LatLng(41.408589, 2.182552),
//     alertType: AlertTypes[AlertTypesNames.MassiveNaziControl]!,
//   ),
//   Alert(
//     time:DateTime.now().millisecondsSinceEpoch,
//     description: "Lot of people in front of the metro station with police national Mossos",
//     location: LatLng (41.403539, 2.19554),
//     alertType: AlertTypes[AlertTypesNames.MassiveNaziControl]!,
//   ),
//   Alert(
//     time: DateTime.now().millisecondsSinceEpoch,
//     description: "A hero officer stopping migrant people on the street",
//     location: LatLng(41.397776, 2.185748),
//     alertType: AlertTypes[AlertTypesNames.SingleAcabAlert]!,
//   ),
//   Alert(
//     time: DateTime.now().millisecondsSinceEpoch,
//     description: "Be carefull they are watching you!",
//     location: LatLng(41.402188, 2.173201),
//     alertType: AlertTypes[AlertTypesNames.SingleAcabAlert]!,
//   ),
// ];