/*
 *  Pareu de Pararme map
 *
 *  Copyright (C) 2021 SOS Racisme
 *
 *  This program is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU Affero General Public License as published by the
 *  Free Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *  FOR A PARTICULAR PURPOSE.
 *  See the GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 *  SPDX-License-Identifier: AGPL-3.0-only
 *
 */

// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'alert_types.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AlertType _$AlertTypeFromJson(Map<String, dynamic> json) {
  return AlertType(
    json['keyId'] as String,
    json['icon'] as String,
    json['alertDescription_es'] as String,
    json['alertTitle_es'] as String,
    json['alertDescription_cat'] as String,
    json['alertTitle_cat'] as String,
  );
}

Map<String, dynamic> _$AlertTypeToJson(AlertType instance) => <String, dynamic>{
      'icon': instance.base64Icon,
      'alertDescription_es': instance.alertDescriptionEs,
      'alertTitle_es': instance.alertTitleEs,
      'alertDescription_cat': instance.alertDescriptionCat,
      'alertTitle_cat': instance.alertTitleCat,
      'keyId': instance.keyId,
    };
