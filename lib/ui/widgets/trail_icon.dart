


/*
 *  Pareu de Pararme map
 *
 *  Copyright (C) 2021 SOS Racisme
 *
 *  This program is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU Affero General Public License as published by the
 *  Free Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *  FOR A PARTICULAR PURPOSE.
 *  See the GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 *  SPDX-License-Identifier: AGPL-3.0-only
 *
 */

import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:pareu_de_pararme_map/AppBuilder.dart';
import 'package:pareu_de_pararme_map/generated/l10n.dart';
import 'package:pareu_de_pararme_map/models/alert.dart';
import 'package:pareu_de_pararme_map/service/alerts.dart';
import 'package:pareu_de_pararme_map/service/storage.dart';
import 'package:pareu_de_pararme_map/ui/constants.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';

/// Function that return the trail icon for the top bar
Widget trailIcon() {
  return Row(
    children: [
      Expanded(
        child: Consumer<AlertsListModel>(
            builder: (context,alertsListModel, child) {
              return IconButton(
                icon: Icon(Icons.update),
                onPressed: () async {
                  final DateTimeRange? picked = await showDateRangePicker(
                    context: context,
                    firstDate: DateTime(2021),
                    lastDate: DateTime.now(),
                    locale: Locale('es', 'CA'),
                    confirmText: S.of(context).update,
                    cancelText: S.of(context).cancel,
                    helpText: S.of(context).showAlertsFrom,
                  );
                  if (picked != null) {
                    alertsUpdateDateRange = DateTimeRange(
                        start: picked.start,
                        // Sum a day, if not, the end date is the begining of the selected day, not inclusive
                        end: DateTime.fromMillisecondsSinceEpoch(
                            picked.end.millisecondsSinceEpoch + 86400000));
                    context.read<AlertsListModel>().add(await getAlerts(
                        rangeDates: alertsUpdateDateRange));
                  }
                },
              );
            }),
      ),
      Expanded(
        child: PopupMenuButton(
          icon: Icon(Icons.more_vert, ),
          itemBuilder: (BuildContext context) => <PopupMenuEntry>[
            PopupMenuItem(
              child: ListTile(
                leading: Icon(Icons.translate),
                title: DropdownButton(
                  hint: Text(
                      S.of(context).language
                  ),
                  icon: Icon(
                    Icons.keyboard_arrow_down_rounded,
                    // color: Colors.,
                  ),
                  isExpanded: true,
                  underline: SizedBox(),
                  // value: valueChoose,
                  onChanged: (Locale? newValue) async {
                    print("Changing locale to $newValue");
                    writeSelectedLocale(newValue.toString()); // Store selected locale for application restart
                    S.load(newValue!);
                    AppBuilder.of(context)!.rebuild();
                  },
                  items: [
                    DropdownMenuItem(
                      value: Locale("es_CA"),
                      child: Text("Català"),
                    ),
                    DropdownMenuItem(
                      value:  Locale("es"),
                      child: Text("Castellano"),
                    )
                  ],
                ),
              ),
            ),
            const PopupMenuDivider(),
            PopupMenuItem(
              child: GestureDetector(
                onTap: () async {
                  if (await canLaunch(FORMALIZE_ALERT_URL))
                    await launch(FORMALIZE_ALERT_URL);
                  else
                    // can't launch url, there is some error
                    throw "Could not launch $FORMALIZE_ALERT_URL";
                },
                child: Text(S.of(context).formalizeComplaint),
              ),
            ),
          ],
        ),
      ),
    ],
  );
}