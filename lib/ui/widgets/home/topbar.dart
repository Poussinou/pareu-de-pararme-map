/*
 *  Pareu de Pararme map
 *
 *  Copyright (C) 2021 SOS Racisme
 *
 *  This program is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU Affero General Public License as published by the
 *  Free Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *  FOR A PARTICULAR PURPOSE.
 *  See the GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 *  SPDX-License-Identifier: AGPL-3.0-only
 *
 */

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:sliding_up_panel/sliding_up_panel.dart';

class TopBar extends StatefulWidget {
  final double? maxHeight;
  final double? minHeight;
  final double? panelAnimationValue;
  final TabController? tabController;
  final PanelController? panelController;

  final String firstTitle, secondTitle;
  double buttonHeight;
  // Title under the icon
  String title;
  IconData leadingIcon;
  late String leadingImage;
  Widget topbarChild;

  Color backgroundColor;
  Color backgroundColor2;
  // Icon located on the top left
  Widget? trailIcon;
  bool disableTrailIcon;

  // Disable shadow under secondTitle when panel is open
  final bool disableBottomShadow;

  TopBar(
      {Key? key,
      this.firstTitle = "",
      this.secondTitle= "",
      this.title = "",
      this.leadingImage = "",
        required this.topbarChild,
        required this.leadingIcon,
        this.backgroundColor = Colors.white,
        this.backgroundColor2 = Colors.white,
        this.trailIcon,
        this.disableTrailIcon=false,
      required this.buttonHeight,
      required this.maxHeight,
      required this.minHeight,
      required this.panelAnimationValue,
      required this.tabController,
      required this.panelController,
      this.disableBottomShadow = false,})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => _TopBarState();
}

// todo: to suport more titles increment the offset programatically, for the moment only two title work
class _TopBarState extends State<TopBar> with SingleTickerProviderStateMixin {
  late Animation<double> _leftHeaderFadeAnimation;
  late Animation<double> _leftHeaderOffsetAnimation;
  late Animation<double> _leftHeaderScaleAnimation;
  late Animation<double> _rightHeaderFadeAnimation;
  late Animation<double> _rightHeaderOffsetAnimation;
  late Animation<double> _rightHeaderScaleAnimation;

  late Animation<double> _headerFadeAnimation;
  late Animation<double> _nameHeaderFadeAnimation;

  late AnimationController _animationController;
  late CurvedAnimation _curvedAnimation;
  late String _firstTitle, _secondTitle, _title;
  late double initialHeight; // This is the initial height of the topbar used to mark minimum image size during animation

  @override
  void initState() {
    super.initState();
    _title = widget.title;
    _firstTitle= widget.firstTitle;
    _secondTitle = widget.secondTitle;

    initialHeight = widget.minHeight! +
        widget.panelAnimationValue! *
            (widget.maxHeight! - 5 * widget.buttonHeight - widget.minHeight! - 20);

    _animationController = AnimationController(vsync: this);
    _curvedAnimation = CurvedAnimation(
        parent: _animationController, curve: Curves.easeInCubic);

    _animationController.value = getPanelAnimationValue!;

    _leftHeaderFadeAnimation = Tween(
      begin: 1.0,
      end: 0.0,
    ).animate(widget.tabController!.animation!);

    _leftHeaderOffsetAnimation = Tween(
      begin: 0.0,
      end: -60.0,
    ).animate(widget.tabController!.animation!);

    _leftHeaderScaleAnimation = Tween(
      begin: 1.0,
      end: 0.5,
    ).animate(widget.tabController!.animation!);

    _rightHeaderFadeAnimation = Tween(
      begin: 0.0,
      end: 1.0,
    ).animate(widget.tabController!.animation!);

    _rightHeaderOffsetAnimation = Tween(
      begin: 60.0,
      end: 0.0,
    ).animate(widget.tabController!.animation!);

    _rightHeaderScaleAnimation = Tween(
      begin: 0.5,
      end: 1.0,
    ).animate(widget.tabController!.animation!);

    _headerFadeAnimation = Tween(
      begin: 1.0,
      end: 0.0,
    ).animate(_curvedAnimation);

    _nameHeaderFadeAnimation = Tween(
      begin: 0.0,
      end: 1.0,
    ).animate(CurvedAnimation(
      parent: _animationController,
      curve: Interval(
        0.5,
        1.0,
        curve: Curves.easeInCubic,
      ),
    ));
  }

  double? get getPanelAnimationValue {
    return widget.panelAnimationValue;
  }

  Widget getHeaderBuilder(BuildContext context, Widget? widget) {
    return Container(
      child: FadeTransition(
        opacity: _headerFadeAnimation,
        child: Stack(
          children: <Widget>[
            // Title1 animation
            Transform(
              transform: Matrix4.translationValues(
                _leftHeaderOffsetAnimation.value,
                0.0,
                0.0,
              ),
              child: ScaleTransition(
                scale: _leftHeaderScaleAnimation,
                child: FadeTransition(
                  opacity: _leftHeaderFadeAnimation,
                  child: Container(
                    child: Text(
                      _firstTitle,
                      style: Theme.of(context).textTheme.title,
                    ),
                  ),
                ),
              ),
            ),
            // title 2 animation
            Transform(
              transform: Matrix4.translationValues(
                _rightHeaderOffsetAnimation.value,
                0.0,
                0.0,
              ),
              child: ScaleTransition(
                scale: _rightHeaderScaleAnimation,
                child: FadeTransition(
                  opacity: _rightHeaderFadeAnimation,
                  child: Container(
                    child: Text(
                      _secondTitle,
                      style: Theme.of(context).textTheme.title,
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    _animationController.value = getPanelAnimationValue!;

    double heightOfTopBar = widget.minHeight! +
        widget.panelAnimationValue! *
            (widget.maxHeight! - 5 * widget.buttonHeight - widget.minHeight! - 20);

    double heightOfNameHeader = 20 * _curvedAnimation.value;

    // Topbar main container
    return Container(
      decoration: BoxDecoration(
        image: DecorationImage(
          image: AssetImage("assets/images/topbanner.png"),
          fit: BoxFit.fill,
          alignment: Alignment.topLeft,
        ),
      ),
      // color:  widget.backgroundColor2,
      // color:  Colors.deepPurple,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          // This containers contain the widgets, is below the title
          Container(
            height: widget.minHeight! +
                (widget.maxHeight! - widget.minHeight!) *
                    widget.panelAnimationValue!,
            // Stack with the main widgets
            child: Stack(
              children: <Widget>[
                Align(
                  alignment: Alignment.bottomCenter,
                  child: Opacity(
                    opacity: widget.panelAnimationValue!,
                    child: Padding(
                      padding: EdgeInsets.symmetric(horizontal: 8.0),
                      child: widget.topbarChild
                    ),
                  ),
                ),
                // Container with the main image/icon/titles
                Container(
                  decoration: BoxDecoration(
                    // color:  widget.backgroundColor,
                    boxShadow: !widget.disableBottomShadow
                      ? <BoxShadow>[
                      BoxShadow(
                        blurRadius: 10,
                        // color: Color.fromRGBO(255, 255, 255, 1.0),
                        color: widget.backgroundColor,
                        spreadRadius: 15,
                      )
                    ]
                    : null,
                  ),
                  // height: heightOfTopBar + heightOfNameHeader,
                  height:(widget.panelController?.isPanelClosed ?? true) &&
                      (((heightOfTopBar + heightOfNameHeader) - 20) > initialHeight) ?
                    (heightOfTopBar + heightOfNameHeader) - 20 : heightOfTopBar + heightOfNameHeader,
                  child: Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            // TITLES BUILDER
                            Expanded(
                              flex: 3,
                              child: Padding(
                                padding: EdgeInsets.symmetric(horizontal: 16.0),
                                child: AnimatedBuilder(
                                  animation: widget.tabController!.animation!,
                                  builder: getHeaderBuilder,
                                ),
                              ),
                            ),
                            // LEADING IMAGE/ICON
                            Expanded(
                              flex: 4,
                              child: Center(
                                child:
                                Center(
                                  child:
                                  Container(
                                    // This conditionals below are needed to decrease the distance with title under the image
                                      width:
                                      (widget.panelController?.isPanelOpen ?? true)
                                          && (widget.panelController?.isPanelAnimating ?? true)
                                          ? heightOfTopBar  : heightOfTopBar + 30,
                                      height: (widget.panelController?.isPanelClosed ?? true)
                                      // This condition below is the minimum height of the leading image
                                          || (((heightOfTopBar - heightOfNameHeader) - 40) < initialHeight)
                                          ? heightOfTopBar  :
                                      (heightOfTopBar - heightOfNameHeader) - 40 ,
                                    decoration: (widget.leadingImage == null)
                                        ? null
                                        : BoxDecoration(
                                      // color: widget.backgroundColor,
                                      // color: Colors.orange,
                                      // borderRadius:
                                      // BorderRadius.circular(
                                      //     heightOfTopBar *
                                      //         0.75 *
                                      //         0.33),
                                      image: DecorationImage(
                                        fit: BoxFit.fitWidth,
                                        image: AssetImage(widget.leadingImage)
                                      ),
                                    ),
                                    child: Visibility(
                                      visible: (widget.leadingImage == null),
                                      child: Center(
                                        child: Icon(
                                          widget.leadingIcon,
                                          size: heightOfTopBar * 0.75,
                                        ),
                                      ),
                                    ),
                                  )
                                ),
                              ),
                            ),
                            // TRAILING ICON
                            Expanded(
                              flex: 3,
                              child: Padding(
                                padding: EdgeInsets.symmetric(horizontal: 16.0),
                                child:  (!widget.disableTrailIcon) ? Align(
                                  alignment: Alignment.centerRight,
                                  child: widget.trailIcon ?? GestureDetector(
                                    onTap: widget.panelController!.isPanelOpen
                                        ? widget.panelController!.close
                                        : widget.panelController!.open,
                                    child: AnimatedIcon(
                                      icon: AnimatedIcons.menu_close,
                                      progress: _curvedAnimation,
                                      size: 30,
                                    ),
                                  ),
                                ) : null,
                              ),
                            ),
                          ],
                        ),
                        // MAIN LEADING TITLE
                        AnimatedBuilder(
                          animation: _curvedAnimation,
                          builder: (BuildContext context, Widget? widget) {
                            return Center(
                              child: Container(
                                // color: Colors.green,
                                height: heightOfNameHeader * 1.2,
                                child: Center(
                                  child: ScaleTransition(
                                    scale: _curvedAnimation,
                                    child: FadeTransition(
                                      opacity: _nameHeaderFadeAnimation,
                                          child: Text(
                                            _title,
                                            style:
                                            Theme.of(context)
                                                .textTheme
                                                .title,
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                );
                              },
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
