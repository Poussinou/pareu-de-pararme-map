/*
 *  Pareu de Pararme map
 *
 *  Copyright (C) 2021 SOS Racisme
 *
 *  This program is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU Affero General Public License as published by the
 *  Free Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *  FOR A PARTICULAR PURPOSE.
 *  See the GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 *  SPDX-License-Identifier: AGPL-3.0-only
 *
 */

import 'package:flutter/material.dart';
import 'package:pareu_de_pararme_map/common.dart';
import 'package:pareu_de_pararme_map/models/alert.dart';
import 'package:pareu_de_pararme_map/ui/widgets/marker.dart';

class AlertCard extends StatelessWidget {
  Alert alert;
  AlertCard(this.alert);

  @override
  Widget build(BuildContext context) {
    return Container(
      constraints: BoxConstraints(minWidth: 100, maxWidth: 300),
      child: Card(
        elevation: 1,
        margin: EdgeInsets.only(bottom: 3),
        child: ListTile(
          leading: AppMarker(
            alert.alertType.icon,
          ),
          title: Text(
            alert.alertType.alertTitle,
            overflow: TextOverflow.fade,
            softWrap: false,
          ),
          subtitle: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              Align(
                child: Text(
                  getFormatedTime(alert.time),
                  // overflow: TextOverflow.fade,
                  // softWrap: false,
                ),
                alignment: Alignment.centerLeft,
              ),
              Align(
                child: Text(
                  alert.description,
                  // overflow: TextOverflow.fade,
                  // softWrap: false,
                ),
                alignment: Alignment.centerLeft,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
