// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a es_CA locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'es_CA';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "addAlert": MessageLookupByLibrary.simpleMessage("Afegeix alerta"),
        "cancel": MessageLookupByLibrary.simpleMessage("Cancel·la"),
        "captchaError":
            MessageLookupByLibrary.simpleMessage("Error resolent el captcha"),
        "formAuthError": MessageLookupByLibrary.simpleMessage(
            "Error de autentificació, reprovi"),
        "formChangeAlertDate": MessageLookupByLibrary.simpleMessage(
            "Seleccioni data de la alerta"),
        "formChangeAlertHour": MessageLookupByLibrary.simpleMessage(
            "Seleccioni la hora de la alerta"),
        "formChangeDate": MessageLookupByLibrary.simpleMessage("Canvia data"),
        "formConnectionError":
            MessageLookupByLibrary.simpleMessage("Error de connexió"),
        "formControlTypeLabel":
            MessageLookupByLibrary.simpleMessage("Tipus de control"),
        "formDescriptionError": MessageLookupByLibrary.simpleMessage(
            "Siusplau, insereixi una descripció d\\\'almenys 5 lletres"),
        "formDescriptionLabel":
            MessageLookupByLibrary.simpleMessage("Descripció"),
        "formFormalizeComplaintQuestion": MessageLookupByLibrary.simpleMessage(
            "Voleu formalitzar la parada?"),
        "formPointNotSelectedError": MessageLookupByLibrary.simpleMessage(
            "Seleccioni un punt al mapa per enviar l\'alerta"),
        "formPointOutsideBounds": MessageLookupByLibrary.simpleMessage(
            "Seleccioni un punt dins de Catalunya"),
        "formPublishAlert":
            MessageLookupByLibrary.simpleMessage("Publicar Alerta"),
        "formalizeComplaint":
            MessageLookupByLibrary.simpleMessage("Formalitzar parada"),
        "insertText":
            MessageLookupByLibrary.simpleMessage("Introdueixi el text"),
        "language": MessageLookupByLibrary.simpleMessage("Idioma"),
        "networkErrorButton": MessageLookupByLibrary.simpleMessage(
            "Error de connexió, reintentar"),
        "ok": MessageLookupByLibrary.simpleMessage("Ok"),
        "select": MessageLookupByLibrary.simpleMessage("Selecciona"),
        "send": MessageLookupByLibrary.simpleMessage("Envia"),
        "showAlertsFrom":
            MessageLookupByLibrary.simpleMessage("Veure alertes des de: "),
        "update": MessageLookupByLibrary.simpleMessage("Actualitza")
      };
}
