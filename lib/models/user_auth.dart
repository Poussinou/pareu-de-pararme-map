/*
 *  Pareu de Pararme map
 *
 *  Copyright (C) 2021 SOS Racisme
 *
 *  This program is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU Affero General Public License as published by the
 *  Free Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *  FOR A PARTICULAR PURPOSE.
 *  See the GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 *  SPDX-License-Identifier: AGPL-3.0-only
 *
 */

import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:pareu_de_pararme_map/service/storage.dart';

class UserAuthProvider extends ChangeNotifier{
  UserAuth _userAuth = UserAuth("","","");

  UserAuth add(String user, String pass, String jwt) {
    writeUser(user);
    writeSecret(pass);
    writeJwt(jwt);
    _userAuth = UserAuth(user, pass, jwt);
    notifyListeners();
    return _userAuth;
  }
  UserAuth get() => _userAuth;
}

class UserAuth {
  String user = "";
  String pass = "";
  String jwt = "";

  UserAuth(this.user, this.pass, this.jwt);

  bool jwtValidate([String? jsonwebtoken]) {
    try  {
      // todo: verify signature using https://pub.dev/packages/jose
      jsonwebtoken ??= this.jwt;
      var jwtSplit = jsonwebtoken.split(".");
      if(jwtSplit.length !=3) {
        return false;
      } else {
        var payload = json.decode(utf8.decode(base64.decode(base64.normalize(jwtSplit[1]))));
        // Check if jwt is no outdated
        if(DateTime.fromMillisecondsSinceEpoch(payload["exp"]*1000).isAfter(DateTime.now())) {
          return true;
        } else {
          return false;
        }
      }
    } catch (e){
      print(e);
      return false;
    }
  }
}

/// This class define captcha request object to GET `/api/register`
class CaptchaReq {
  String captchaId, captchaResolution, phoneId;
  CaptchaReq(this.captchaId, this.captchaResolution, this.phoneId);

  Map<String, dynamic> toJson( ) => <String, dynamic>{
    'captchaId': this.captchaId,
    'captchaResolution': this.captchaResolution,
    'phoneId': this.phoneId,
  };
}
