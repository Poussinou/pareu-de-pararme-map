/*
 *  Pareu de Pararme map
 *
 *  Copyright (C) 2021 SOS Racisme
 *
 *  This program is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU Affero General Public License as published by the
 *  Free Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *  FOR A PARTICULAR PURPOSE.
 *  See the GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 *  SPDX-License-Identifier: AGPL-3.0-only
 *
 */

import 'dart:io' as io;
import 'package:device_info_plus/device_info_plus.dart';
import 'package:flutter/material.dart';
import 'package:pareu_de_pararme_map/models/user_auth.dart';
import 'package:pareu_de_pararme_map/ui/constants.dart';
import 'dart:convert';
import 'package:pareu_de_pararme_map/generated/l10n.dart';
import 'package:crypto/crypto.dart';

class CaptchaController {
  String b64captcha = "";
  String captchaId = "";

  void fromJson(Map<String, dynamic> captchaData) {
    b64captcha = captchaData["image"];
    captchaId = (captchaData["id"] as int ).toString();
  }
}

class CaptchaWidget extends StatefulWidget {
  CaptchaController captchaController;
  Function(CaptchaReq) cb;
  CaptchaWidget(this.captchaController, this.cb);

  @override
  _CaptchaWidgetState createState() => _CaptchaWidgetState();
}

class _CaptchaWidgetState extends State<CaptchaWidget> {

  bool _isSolving = false; // Check if the captcha is in "solving" state
  String identifier = "";
  TextEditingController _teCaptcha = TextEditingController();

  void _getIdentifier() async {
    try {
      DeviceInfoPlugin _deviceInfo = DeviceInfoPlugin();
      if (io.Platform.isAndroid) {
        identifier = (await _deviceInfo.androidInfo).androidId!;
      } else if (io.Platform.isIOS) {
        identifier = (await _deviceInfo.iosInfo).identifierForVendor!;
      }
      identifier = md5.convert(utf8.encode(identifier)).toString();
    } catch (e){
      identifier = "cantresolveidentifier_${DateTime.now().millisecondsSinceEpoch}";
    }
    print("Solved unique identifier: $identifier");
  }

  Future<void> _solveCaptcha() async {
    print("Solving Captcha, identifier $identifier");
    if (!_isSolving) {
      if(_teCaptcha.text.isEmpty) {
        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
          content: Text(S.of(context).insertText),
        ));
        return;
      }
      setState(() {_isSolving = true;});
      if (identifier.isNotEmpty && _teCaptcha.text.isNotEmpty) {
        await widget.cb(CaptchaReq(widget.captchaController.captchaId, _teCaptcha.text, identifier));
      }
    }
  }

  @override
  void initState() {
    _getIdentifier();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Image.memory(base64.decode(widget.captchaController.b64captcha)),
          SizedBox(height: 20,),
          Card(
            child: SizedBox(
              height: 40.0,
              width: 200.0,
              child: TextField(
                controller: _teCaptcha,
                decoration: InputDecoration(
                  enabledBorder: OutlineInputBorder(
                    borderSide: const BorderSide(color: PRIMARY_BTN_COLOR, width: 0.0),
                  ),
                  focusedBorder: OutlineInputBorder(
                    borderSide: const BorderSide(color: PRIMARY_HOVER_BTN_COLOR, width: 0.0),
                  ),
                  labelStyle: TextStyle(
                      color: PRIMARY_BTN_COLOR,
                      // backgroundColor: Colors.yellow
                  ),
                  labelText: 'Captcha',
                ),
              ),
            ),
          ),
          ElevatedButton(
            style:  ButtonStyle(
              backgroundColor: MaterialStateProperty.all<Color>(
                  _isSolving ? DISABLED : PRIMARY_BTN_COLOR
              ),
            ),
            onPressed: () async {
              try {
                await _solveCaptcha();
              } catch (e){
                // If error give the possibility to send another captcha
                setState(() {_isSolving = false;});
                rethrow;
              }
            },
            child: Text(S.of(context).send),
          ),
        ],
      ),
    );
  }
}
