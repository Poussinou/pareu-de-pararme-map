

/*
 *  Pareu de Pararme map
 *
 *  Copyright (C) 2021 SOS Racisme
 *
 *  This program is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU Affero General Public License as published by the
 *  Free Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *  FOR A PARTICULAR PURPOSE.
 *  See the GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 *  SPDX-License-Identifier: AGPL-3.0-only
 *
 */

import 'package:pareu_de_pararme_map/ui/constants.dart';

const JWT = "jwt", USER = "user", SECRET = "secret", LOCALE = "locale";

Future<String> get jwtOrEmpty async => await storage.read(key: JWT) ?? "";
writeJwt (String jwt) => storage.write(key: JWT, value: jwt);
Future<String> get userOrEmpty async => await storage.read(key: USER) ?? "";
writeUser (String user) => storage.write(key: USER, value: user);
Future<String> get passOrEmpty async => await storage.read(key: SECRET) ?? "";
writeSecret (String secret) => storage.write(key: SECRET, value: secret);

writeSelectedLocale(String locale) => storage.write(key: LOCALE, value: locale);
Future<String> get localeOrEmpty async => await storage.read(key: LOCALE) ?? "";



