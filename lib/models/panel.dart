

/*
 *  Pareu de Pararme map
 *
 *  Copyright (C) 2021 SOS Racisme
 *
 *  This program is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU Affero General Public License as published by the
 *  Free Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *  FOR A PARTICULAR PURPOSE.
 *  See the GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 *  SPDX-License-Identifier: AGPL-3.0-only
 *
 */

import 'package:flutter/material.dart';
import 'package:pareu_de_pararme_map/ui/constants.dart' show panelController, popupLayerController;

/// Notifier used to manage the state of the panel
///
/// To notify if its open, close etc...
class PanelModel extends ChangeNotifier {
  /// Internal, private state of the panel
  bool _isOpen = false;

  /// An unmodifiable view of the items
  bool get isOpen => _isOpen;

  /// Open the panel using controller
  void open() {
    panelController.open();
    setOpenedState();
  }

  /// Set app global state as panel open. This is need because you can
  /// open/close the panel using gesture, no only the button
  void setOpenedState() {
    popupLayerController.hidePopup();
    _isOpen = true;
    notifyListeners();
  }

  /// Removes all items from the cart.
  void close() {
    panelController.close();
    setCloseState();
  }

  void setCloseState(){
    _isOpen = false;
    notifyListeners();
  }
}