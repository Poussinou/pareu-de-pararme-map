/*
 * Pareu de Pararme map
 *
 * Copyright (C) 2021 SOS Racisme
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the
 * Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU Affero General Public License for more details.
 * You should have received a copy of the GNU Affero General Public License
 * along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 * SPDX-License-Identifier: AGPL-3.0-only
 */

import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:pareu_de_pararme_map/ui/constants.dart';

class FireBaseInitialization extends StatefulWidget {
  final Widget child;
  const FireBaseInitialization({required this.child, Key? key}) : super(key: key);

  @override
  _FireBaseInitializationState createState() => _FireBaseInitializationState();
}

/// We are using a StatefulWidget such that we only create the [Future] once,
/// no matter how many times our widget rebuild.
/// If we used a [StatelessWidget], in the event where [App] is rebuilt, that
/// would re-initialize Flutter
class _FireBaseInitializationState extends State<FireBaseInitialization> {
  final Future<FirebaseApp> _initialization = Firebase.initializeApp();
  FirebaseMessaging? messaging;
  
  Future<void> _initializeMessaging() async {
    if(messaging == null) {
      messaging = FirebaseMessaging.instance;
      messaging!.subscribeToTopic(FCM_NEW_ALERT);
      FirebaseMessaging.onBackgroundMessage(
          _firebaseMessagingBackgroundHandler);
    }
  }

  @override
  void initState() {
    print("Initializing firebase");
    // TODO: implement initState
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      // Initialize FlutterFire:
      future: _initialization,
      builder: (context, snapshot) {
        // Check for errors
        if (snapshot.hasError) {
          return Text("Firebase error");
        }

        // Once complete, show your application
        if (snapshot.connectionState == ConnectionState.done) {
          _initializeMessaging();
          return widget.child;
        }

        // Otherwise, show something whilst waiting for initialization to complete
        return CircularProgressIndicator();
      },
    );
  }
}

Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  // If you're going to use other Firebase services in the background, such as Firestore,
  // make sure you call `initializeApp` before using other Firebase services.
  // await Firebase.initializeApp();
  print("Handling a background message: ${message.messageId} ${message.data}");
}
