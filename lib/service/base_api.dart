
/*
 *  Pareu de Pararme map
 *
 *  Copyright (C) 2021 SOS Racisme
 *
 *  This program is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU Affero General Public License as published by the
 *  Free Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *  FOR A PARTICULAR PURPOSE.
 *  See the GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 *  SPDX-License-Identifier: AGPL-3.0-only
 *
 */

import 'dart:convert';

import 'package:pareu_de_pararme_map/ui/constants.dart';
import 'package:http/http.dart' as http;

getJwt(jwtString) => "Bearer $jwtString";

Future<Map<String, dynamic>> apiCall(
    String path, {
      Map<String, dynamic>? params,
      String jwtString = "",
    }) async {
  final url = Uri.parse(API_URL + path);
  final header = jwtString.isNotEmpty ? <String, String>{'Authorization': getJwt(jwtString)} : <String, String>{}
    ..addAll({"Accept" : "application/json, text/plain, */*"})
    ..addAll({"Content-Type" : "application/json; charset=utf-8"});
  http.Response response;

  print("Trying API call to $url ");
  // print("$jwtString params: " + jsonEncode(params));

  try {
    response = params == null
        ? await http.get(url, headers: header,)
        : await http.post(url, body: jsonEncode(params), headers: header,);
  } catch(e) {
    print('Request failed: ' + url.toString() );
    throw e;
  }

  print("Response get with status code: ${response.statusCode}");

  switch (response.statusCode) {
    case 200:
      var bodyBytes = jsonDecode(utf8.decode(response.bodyBytes));
      // print("And body bytes: $bodyBytes");
      // Prevent pure list responses
      return bodyBytes is List<dynamic>
          ? { "res" : bodyBytes } : bodyBytes;
    case 401:
      throw BannedException();
    case 403:
      throw BannedException();
    default:
      print(response.body);
      throw ("Server is drunk with status code ${response.statusCode}");
  }
}

/// Throw when unauthenticated
class BannedException implements Exception {
  String errMsg() => 'Seems you are banned fking C00p';
}
