
/*
 *  Pareu de Pararme map
 *
 *  Copyright (C) 2021 SOS Racisme
 *
 *  This program is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU Affero General Public License as published by the
 *  Free Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *  FOR A PARTICULAR PURPOSE.
 *  See the GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 *  SPDX-License-Identifier: AGPL-3.0-only
 *
 */

import 'package:flutter/cupertino.dart';
import 'package:pareu_de_pararme_map/models/user_auth.dart';
import 'package:pareu_de_pararme_map/service/base_api.dart' show apiCall;
import 'package:provider/provider.dart';

/// With GET you get a captcha, POST for register
const _REGISTER = '/api/register';
const _LOGIN = '/api/login';

/// Get a captcha from the server
///
/// Response:
/// ```
/// {
///   "id": 1,
///   "image": "1234" # This will be base64 blob
/// }
///```
Future<Map<String, dynamic>> captcha( ) async =>
    await apiCall(_REGISTER,);

/// Signup, need a captcha resolution
Future<Map<String, dynamic>> signup(CaptchaReq req) async =>
    await apiCall(_REGISTER,
      params: req.toJson(),
    );

/// Login
Future<Map<String, dynamic>> login(String username, String password,) async =>
    await apiCall(_LOGIN,
      params: {
        "phoneId": username,
        "password": password,
      },
    );

/// Do login and update storage token
///
/// [userAuth] is the actual user auth data, the [context] is needed to store it
/// on the app userAuth provider
Future<void> updateToken(BuildContext context, UserAuth userAuth,) async {
  var res = await login(userAuth.user, userAuth.pass);
  setUserInfo(context, userAuth.user, userAuth.pass, res["access_token"]);
}

/// Set user auth info in the provider context to be used over the whole app
UserAuth setUserInfo(BuildContext context,String user, String pass, String jwt) {
  // print("Seting user info $user, $pass, $jwt");
  return Provider.of<UserAuthProvider>(context, listen: false).add(user, pass, jwt);
}
