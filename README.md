# pareu_de_pararme_map

https://www.pareudepararme.org/


### CAST:

"Pareu de parar-me" busca informar y recoger datos sobre las paradas policiales por perfil racial.
¿Fuiste abordado por la policía o eres testigo de una parada policial racista? Sube al mapa la hora, el local y describe lo que ha pasado. Te invitamos también a formalizar la denuncia utilizando el formulario.

* **Identificación personal**: Parada individual por policial paisano o identificado
* **Redada policial**: Operación policial masiva que identifica a personas por su perfil racial

Esta app sólo es posible gracias a la colaboración colectiva de muchas personas.  Úsala con responsabilidad.



### CAT:

"Pareu de parar-me" busca informar i recollir dades sobre les parades policials per perfil racial.
Vas ser abordat per la policia o ets testimoni d'una parada policial racista? Puja al mapa l'hora, el local i descriu el que ha passat. Et convidem també a formalitzar la denúncia utilitzant el formulari.

* **Identificació personal**: Parada individual per policial paisà o identificat
* **Batuda policial**: Operació policial massiva que identifica a persones pel seu perfil racial

Aquesta app només és possible gràcies a la col·laboració col·lectiva de moltes persones. Usa-la amb responsabilitat.
