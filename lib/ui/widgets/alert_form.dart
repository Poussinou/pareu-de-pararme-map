/*
 *  Pareu de Pararme map
 *
 *  Copyright (C) 2021 SOS Racisme
 *
 *  This program is free software: you can redistribute it and/or modify it under
 *  the terms of the GNU Affero General Public License as published by the
 *  Free Software Foundation, version 3.
 *
 *  This program is distributed in the hope that it will be useful, but WITHOUT
 *  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 *  FOR A PARTICULAR PURPOSE.
 *  See the GNU Affero General Public License for more details.
 *
 *   You should have received a copy of the GNU Affero General Public License
 *  along with this program. If not, see <https://www.gnu.org/licenses/>
 *
 *  SPDX-License-Identifier: AGPL-3.0-only
 *
 */

import 'package:flutter/material.dart';
import 'package:pareu_de_pararme_map/models/alert_types.dart';
import 'package:pareu_de_pararme_map/models/user_auth.dart';
import 'package:pareu_de_pararme_map/service/alerts.dart';
import 'package:pareu_de_pararme_map/service/base_api.dart';
import 'package:pareu_de_pararme_map/ui/widgets/marker.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:pareu_de_pararme_map/models/alert.dart';
import 'package:pareu_de_pararme_map/models/panel.dart';
import 'package:provider/provider.dart';
import 'package:pareu_de_pararme_map/generated/l10n.dart';

import '../constants.dart';
import 'alert_card.dart';

class AlertForm extends StatefulWidget {
  @override
  _AlertFormState createState() => _AlertFormState();
}

class _AlertFormState extends State<AlertForm> {
  // String alertDropdownValue = AlertTypesList[0].keyId;
  AlertType? alertDropdownValue;

  TextEditingController _teDescription = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  bool _creatingAlert = false; // Used to know when Ok button is pressed to don't send same alert twice

  @override
  void initState() {
    alertDropdownValue = context.read<AlertTypesProvider>().alertTypes[0];
    // TODO: implement initState
    super.initState();
  }
  
  // Used to show the tooltip ontap
  final _tooltipKey = GlobalKey<State<Tooltip>>();
  
  @override
  Widget build(BuildContext context) {
    return Consumer<AlertTypesProvider>(
      builder: (context, alertTypes, child) =>
          Container(
            // color: Colors.white,
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage("assets/images/cop.png"),
                fit: BoxFit.none,
                scale: 1.8,
                alignment: Alignment.centerLeft,
              ),
            ),
            child: Form(
              key: _formKey,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Card(
                    elevation: 1,
                    margin: EdgeInsets.only(bottom: 3),
                    child: ListTile(
                      leading: Tooltip(
                        key: _tooltipKey,
                        // textStyle: const TextStyle(
                        //   fontSize: 24,
                        // ),
                        message: "${S.of(context).formControlTypeLabel} - "
                            "${alertDropdownValue!.alertTitle}  \n"
                            "${alertDropdownValue!.alertDescription}",
                        child: IconButton(
                          icon: Icon(Icons.info_outline),
                          onPressed: () {
                            final dynamic tooltip = _tooltipKey.currentState;
                            tooltip.ensureTooltipVisible();
                          },
                        ),
                      ),
                      // trailing: Text(S.of(context).formControlTypeLabel),
                      // contentPadding: EdgeInsets.fromLTRB(10, 0, 10, 0),
                      title: DropdownButtonHideUnderline(
                        child: DropdownButton<AlertType>(
                          isExpanded: false,
                          value: alertDropdownValue,
                          items: alertTypes.alertTypes.map((item) {
                            return DropdownMenuItem(
                              child: Container(
                                width: 200,                    //expand here
                                child: Row(
                                  children: [
                                    Expanded(child:
                                    AppMarker(item.icon, )
                                    ),
                                    Expanded(
                                      child: Text(
                                        item.alertTitle,
                                        textAlign: TextAlign.end,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              value: item,
                            );
                          }).toList(),
                          onChanged: (AlertType? newValue) {
                            setState(() {
                              alertDropdownValue = newValue!;
                            });
                          },
                          hint: Container(
                            width: 150,
                            child: Text(
                              "Select Item Type",
                              style: TextStyle(color: Colors.grey),
                              textAlign: TextAlign.end,
                            ),
                          ),
                          style:
                          TextStyle(color: Colors.black, decorationColor: Colors.red),
                        ),
                      ),
                    ),
                  ),
                  Card(
                    elevation: 1,
                    margin: EdgeInsets.only(bottom: 3),
                    child: ListTile(
                      leading: Icon(Icons.description),
                      title: TextFormField(
                        controller: _teDescription,
                        validator: (value) {
                          if (value == null || value.isEmpty || value.length < 5) {
                            return S.of(context).formDescriptionError;
                          }
                          return null;
                        },
                        decoration: InputDecoration(
                          // border: UnderlineInputBorder(),
                            labelText: S.of(context).formDescriptionLabel
                        ),
                      ),
                    ),
                  ),
                  ButtonTheme(
                    minWidth: 70,
                    height: 70,
                    child: ElevatedButton.icon(
                      style: ButtonStyle(
                        backgroundColor: MaterialStateProperty.all<Color>(SECONDARY_NAV_COLOR),
                      ),
                      icon: Icon(Icons.campaign, color: Colors.black,),
                      label: Text(S.of(context).send, style: TextStyle(color: Colors.black),
                      ),
                      onPressed: () {
                        if(Provider.of<NewAlertModel>(context, listen: false).newAlert == null) {
                          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                            content: Text(S.of(context).formPointNotSelectedError),
                          ));
                        }
                        else if (_formKey.currentState!.validate()) {
                          var loc = Provider.of<NewAlertModel>(context, listen: false).newAlert!.location;

                          var newAlert = Alert(
                            time: DateTime.now().millisecondsSinceEpoch,
                            alertType: (alertDropdownValue)!,
                            description: _teDescription.text,
                            lat:  loc.latitude,
                            lon:  loc.longitude,
                          );
                          _showAcceptAlertDialog(context, newAlert);
                        }
                      },
                    ),
                  ),
                ],
              ),
            ),
          ),
    );
  }

  _resetForm(Function removeNewAlertPoint){
    _teDescription.text = "";
    _formKey.currentState!.reset();
    removeNewAlertPoint();
    context.read<PanelModel>().close();
  }

  /// Function that show the send alert acceptance dialog
  _showAcceptAlertDialog(BuildContext context, Alert newAlert) {
    // Used to delete the point on the map after send the alert succesfully
    var removeNewAlertPoint = context.read<NewAlertModel>( ).remove;
    showDialog<String>(
      context: context,
      builder: (BuildContext context) => StatefulBuilder( // Osed to set state inside the dialog and update the alert
        builder: (context, setState) => AlertDialog(
          title: Text(
            S.of(context).formPublishAlert,
            style: TextStyle(color: Colors.black),
          ),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.start,
            children: [
              AlertCard(newAlert),
              GestureDetector(
                onTap: () async {
                  var newTime = await _showDateTimeToEpochPicker();
                  if(newTime != null) setState(() {
                    newAlert.time = newTime;
                  });
                },
                child: Text(
                  S.of(context).formChangeDate,
                  style: TextStyle(
                    fontSize: 15.0,
                    decoration: TextDecoration.underline,
                    fontStyle: FontStyle.italic,
                    color: LINK,
                  ),
                ),
              ),
            ],
          ),
          actions: <Widget>[
            TextButton(
              onPressed: () => Navigator.pop(context, 'Cancel'),
              child: Text(S.of(context).cancel),
            ),
            TextButton(
              onPressed: () async {
                if(!_creatingAlert) {
                  _creatingAlert = true;
                  try {
                    await createAlert( context, context.read<UserAuthProvider>().get(),newAlert,);
                  } on BannedException {
                    _creatingAlert = false;
                    Navigator.pop(context, 'Cancel');
                    _resetForm(removeNewAlertPoint);
                    Navigator.pushReplacementNamed(context, '/landing');
                    ScaffoldMessenger.of(context).showSnackBar(
                        SnackBar(content:
                          Text(S.of(context).formAuthError),
                        ));
                    rethrow;
                  } catch(e){
                    _creatingAlert = false;
                    Navigator.pop(context, 'Cancel');
                    ScaffoldMessenger.of(context).showSnackBar(
                        SnackBar( content:
                          Text(S.of(context).formConnectionError),
                        ));
                    print("error creating alert");
                    rethrow;
                  }
                  // If alert created successfully
                  // Add the created alert to the map
                  var alertsListModel = context.read<AlertsListModel>();
                  alertsListModel.add(alertsListModel.alerts..add(newAlert));
                  // Reset form and pop
                  _resetForm(removeNewAlertPoint);
                  Navigator.pop(context, 'OK');
                  // Show formalize dialog
                  _showFormalizeAlertDialog();
                }
                _creatingAlert = false;
              },
              child: Text(S.of(context).ok),
            ),
          ],
        ),
      ),
    );
  }

  /// This dialog show "formalize alert" text, which basically redirect to the
  /// `pareudepararme.org` form to formalize a complaint
  _showFormalizeAlertDialog(){
    showDialog<String>(
      context: context,
      builder: (BuildContext context) => AlertDialog(
        title: Text(
          S.of(context).formalizeComplaint,
          style: TextStyle(color: Colors.black),
        ),
        content: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Text(
              S.of(context).formFormalizeComplaintQuestion,
            ),
          ],
        ),
        actions: <Widget>[
          TextButton(
            onPressed: () => Navigator.pop(context, 'Cancel'),
            child: Text(S.of(context).cancel),
          ),
          TextButton(
            onPressed: () async {
              if (await canLaunch(FORMALIZE_ALERT_URL))
                await launch(FORMALIZE_ALERT_URL);
              else
                // can't launch url, there is some error
                throw "Could not launch $FORMALIZE_ALERT_URL";
              Navigator.pop(context, 'OK');
            },
            child: Text(S.of(context).ok),
          ),
        ],
      ),
    );
  }

  /// Show DatePicker and if success TimePicker
  ///
  /// It return the selected time on millisecondsSinceEpoch (int)
  Future<int?> _showDateTimeToEpochPicker() async {
    final DateTime? datePicked = await showDatePicker(
      context: context,
      initialDate: DateTime.now(),
      firstDate: DateTime(2021),
      lastDate: DateTime.now(),
      locale: Locale('es', 'CA'),
      confirmText: S.of(context).select,
      cancelText: S.of(context).cancel,
      helpText: S.of(context).formChangeAlertDate,
    );
    if(datePicked != null) {
      final TimeOfDay? timePicked = await showTimePicker(
        context: context,
        initialTime: TimeOfDay.now(),
        helpText: S.of(context).formChangeAlertHour,
        confirmText: S.of(context).select,
        cancelText: S.of(context).cancel,
      );
      if(timePicked != null) {
        return DateTime(
            datePicked.year, datePicked.month, datePicked.day, timePicked.hour, timePicked.minute
        ).millisecondsSinceEpoch;
      }
    }
    return null;
  }
}